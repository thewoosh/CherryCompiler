/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * http://www.unicode.org/charts/PDF/UFE70.pdf
 */

#pragma once

#include "../Base.hpp"

namespace text {

    constexpr CodePoint ZERO_WIDTH_NO_BREAK_SPACE = 0xFEFF; // ZWNBSP, BOM

} // namespace text
