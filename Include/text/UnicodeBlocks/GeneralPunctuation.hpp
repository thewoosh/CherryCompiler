/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * https://www.unicode.org/charts/PDF/U2000.pdf
 */

#pragma once

#include "../Base.hpp"

namespace text {

    // Format Characters
    constexpr CodePoint ZERO_WIDTH_SPACE = 0x200B; // ZWSP
    constexpr CodePoint ZERO_WIDTH_NON_JOINER = 0x200C; // ZWNJ
    constexpr CodePoint ZERO_WIDTH_JOINER = 0x200D; // ZWJ
    constexpr CodePoint LEFT_TO_RIGHT_MARK = 0x200E; // LRM
    constexpr CodePoint RIGHT_TO_LEFT_MARK = 0x200E; // RLM

    // ...

    // Separators
    constexpr CodePoint LINE_SEPARATOR = 0x2028;
    constexpr CodePoint PARAGRAPH_SEPARATOR = 0x2029;

} // namespace text
