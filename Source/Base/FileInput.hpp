/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <cstdio>

namespace base {

    class FileInput {
        std::FILE *m_file{nullptr};
        std::size_t m_size{static_cast<std::size_t>(-1)};

    public:
        [[nodiscard]] explicit
        FileInput(const std::string &fileName) noexcept;

        ~FileInput() noexcept;

        [[nodiscard]] bool
        isEOF() const noexcept;

        [[nodiscard]] inline bool
        isOK() const noexcept {
            return m_file != nullptr;
        }

        [[nodiscard]] bool
        readChar(char32_t *out) noexcept;

        [[nodiscard]] inline constexpr std::size_t
        size() const noexcept {
            return m_size;
        }
    };

} // namespace base
