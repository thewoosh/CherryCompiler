/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Base/Logger.hpp"

#include <array>
#include <chrono>

#include <fmt/chrono.h>

#include "Source/Base/Assert.hpp"

std::string_view
logger::currentDateTime() noexcept {
    static thread_local std::string buffer;

    buffer.clear();
    fmt::format_to(std::back_inserter(buffer), "{:%H:%M:%S}", std::chrono::system_clock::now().time_since_epoch());

    return {std::data(buffer)};
}
