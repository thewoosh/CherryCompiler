/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Base/Logger.hpp"

#include <cstdlib> // for std::abort

#undef CHERRY_ASSERT

#ifdef __PRETTY_FUNCTION__
#   define CHERRY_FUNCTION __PRETTY_FUNCTION__
#elif defined(__FUNCSIG__)
#   define CHERRY_FUNCTION __FUNCSIG__
#else
#   define CHERRY_FUNCTION __func__
#endif

#ifdef NDEBUG
#   define CHERRY_ASSERT(e) ((void)0)
#   define CHERRY_ASSERT_NOT_REACHED_OR_RETURN(retval) return retval
#else
#   define CHERRY_ASSERT(e) \
        do {                \
            if (!(e)) {          \
                logger::error("Assertion failed: {}", #e); \
                logger::error("File: {}:{}", __FILE__, __LINE__); \
                logger::error("Function: {}", CHERRY_FUNCTION);   \
                std::abort(); \
            } \
        } while (0)
#   define CHERRY_ASSERT_NOT_REACHED_OR_RETURN(retval)    \
        static_cast<void>(retval);                        \
        logger::error("Unreachable code reached!");       \
        logger::error("File: {}:{}", __FILE__, __LINE__); \
        logger::error("Function: {}", CHERRY_FUNCTION);   \
        std::abort()
#endif
