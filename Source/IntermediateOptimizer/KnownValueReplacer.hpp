/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <map>

#include <cstdint>

#include "Source/IntermediateOptimizer/SubOptimizer.hpp"

namespace intermediate::optimization {

    class KnownValueReplacer
            : public SubOptimizer {
    public:
        [[nodiscard]] inline explicit
        KnownValueReplacer(FunctionBlock &block) noexcept
                : SubOptimizer(block) {
        }

        void
        run() noexcept;

    private:
        template <typename Operation>
        void
        inspectOp(Instruction &, Operation operation) noexcept;

        void
        inspectMove(Instruction &) noexcept;

        void
        inspectReturn(Instruction &) noexcept;

        std::map<Register, std::uint64_t> knownValues{};
    };

} // namespace intermediate::optimization
