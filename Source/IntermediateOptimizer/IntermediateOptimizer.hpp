/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Base/CompilerFeatures.hpp"
#include "Source/IntermediateGenerator/FunctionBlock.hpp"

namespace intermediate {

    class Optimizer {
    public:
        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR explicit
        Optimizer(std::vector<FunctionBlock> &blocks) noexcept
                : m_blocks(blocks) {
        }

        void
        run() noexcept;

    private:
        /**
         * @returns true when something has changed, false otherwise
         */
        [[nodiscard]] static bool
        inspect(FunctionBlock &block) noexcept;

        std::vector<FunctionBlock> &m_blocks;
    };

} // namespace intermediate
