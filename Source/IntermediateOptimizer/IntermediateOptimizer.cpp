/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IntermediateOptimizer/IntermediateOptimizer.hpp"

#include "Source/IntermediateOptimizer/DeadCodeRemover.hpp"
#include "Source/IntermediateOptimizer/KnownValueReplacer.hpp"

namespace intermediate {

    bool
    Optimizer::inspect(FunctionBlock &block) noexcept {
        bool hasChanged{false};

        optimization::KnownValueReplacer knownValueReplacer{block};
        knownValueReplacer.run();
        if (knownValueReplacer.hasSomethingChanged())
            hasChanged = true;

        optimization::DeadCodeRemover deadCodeRemover{block};
        deadCodeRemover.run();
        if (deadCodeRemover.hasSomethingChanged())
            hasChanged = true;

        return hasChanged;
    }

    void
    Optimizer::run() noexcept {
        constexpr const std::size_t repeatMax{10};
        for (auto &functionBlock : m_blocks) {
            std::size_t repeatCount{0};
            while (inspect(functionBlock)) {
                if (repeatCount++ == repeatMax) {
                    logger::warning("IntermediateOptimizer: function \"{}\" reached maximum iterations of {}",
                                    functionBlock.name, repeatMax);
                    break;
                }
            }

            logger::info("IntermediateOptimizer: function \"{}\" resulted in {} instruction(s).", functionBlock.name,
                         std::size(functionBlock.instructions));
            for (const auto &instruction : functionBlock.instructions) {
                if (instruction.lhs.index() != 0) {
                    if (instruction.rhs.index() != 0) {
                        logger::info("    {} {}, {}", toString(instruction.type), instruction.lhs.toString(),
                                     instruction.rhs.toString());
                    } else {
                        logger::info("    {} {}", toString(instruction.type), instruction.lhs.toString());
                    }
                } else {
                    logger::info("    {}", toString(instruction.type));
                }
            }
        }
    }

} // namespace intermediate
