/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IntermediateOptimizer/KnownValueReplacer.hpp"

namespace intermediate::optimization {

    void
    KnownValueReplacer::run() noexcept {
        for (auto &instruction : block().instructions) {
            switch (instruction.type) {
                case InstructionType::ADD:
                    inspectOp(instruction, [](auto lhs, auto rhs) { return lhs + rhs; });
                    break;
                case InstructionType::DIVIDE:
                    inspectOp(instruction, [](auto lhs, auto rhs) { return lhs / rhs; });
                    break;
                case InstructionType::MOVE:
                    inspectMove(instruction);
                    break;
                case InstructionType::MULTIPLY:
                    inspectOp(instruction, [](auto lhs, auto rhs) { return lhs * rhs; });
                    break;
                case InstructionType::RETURN:
                    inspectReturn(instruction);
                    break;
                case InstructionType::SUBTRACT:
                    inspectOp(instruction, [](auto lhs, auto rhs) { return lhs - rhs; });
                    break;
                default:
                    logger::warning("IntermediateOptimizer: unknown instruction type {}", toString(instruction.type));
                    break;
            }
        }
    }

    template <typename Operation>
    void
    KnownValueReplacer::inspectOp(Instruction &instruction, Operation operation) noexcept {
        auto it = knownValues.find(std::get<Register>(instruction.lhs));
        if (it == std::end(knownValues)) {
            std::uint64_t value = 0xFFFFFFFFFFFFFFFF;
            if (instruction.rhs.isImmediateUnsigned()) {
                value = std::get<std::uint64_t>(instruction.rhs);
            } else {
                auto rhsIt = knownValues.find(std::get<Register>(instruction.rhs));
                if (it == std::end(knownValues)) {
                    return;
                }
                value = rhsIt->second;
            }

            if (value == 0 && instruction.type == InstructionType::MULTIPLY) {
                knownValues.emplace(std::get<Register>(instruction.lhs), 0);
            }

            return;
        }

        if (instruction.rhs.isImmediateUnsigned()) {
            it->second = operation(it->second, std::get<std::uint64_t>(instruction.rhs));
        } else if (instruction.rhs.isRegister()) {
            auto rhsIt = knownValues.find(std::get<Register>(instruction.rhs));
            if (it != std::end(knownValues)) {
                it->second = operation(it->second, rhsIt->second);
            }
        }
    }

    void
    KnownValueReplacer::inspectMove(Instruction &instruction) noexcept {
        const auto reg = std::get<Register>(instruction.lhs);
        auto it = knownValues.find(reg);

        std::uint64_t value{};
        if (instruction.rhs.isImmediateUnsigned()) {
            value = std::get<std::uint64_t>(instruction.rhs);
        } else if (instruction.rhs.isRegister()) {
            it = knownValues.find(std::get<Register>(instruction.rhs));
            if (it == std::end(knownValues))
                return;
            value = it->second;
        }

        if (it == std::end(knownValues)) {
            knownValues.emplace(reg, value);
        } else {
            it->second = value;
        }
    }

    void
    KnownValueReplacer::inspectReturn(Instruction &instruction) noexcept {
        if (!instruction.lhs.isRegister())
            return;

        auto it = knownValues.find(std::get<Register>(instruction.lhs));
        if (it == std::end(knownValues))
            return;

        instruction.lhs = it->second;
        markSomethingChanged();
    }

} // namespace intermediate::optimization