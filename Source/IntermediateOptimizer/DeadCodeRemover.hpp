/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <map>

#include <cstdint>

#include "Source/IntermediateOptimizer/SubOptimizer.hpp"

namespace intermediate::optimization {

    class DeadCodeRemover
            : public SubOptimizer {
    public:
        [[nodiscard]] inline explicit
        DeadCodeRemover(FunctionBlock &block) noexcept
                : SubOptimizer(block) {
        }

        void
        run() noexcept;

        struct Usage {
            bool definitelyUsed{false};

            // Registers that rely on this register
            std::vector<Usage *> dependent{};
        };

    private:
        void
        inspectGeneral(Instruction &) noexcept;

        void
        inspectMove(Instruction &) noexcept;

        void
        inspectReturn(Instruction &) noexcept;

        std::map<Register, Usage> m_usedRegisters{};
    };

} // namespace intermediate::optimization
