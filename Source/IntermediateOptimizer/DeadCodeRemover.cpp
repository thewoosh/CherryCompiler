/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IntermediateOptimizer/DeadCodeRemover.hpp"

namespace intermediate::optimization {

    void
    DeadCodeRemover::inspectGeneral(Instruction &instruction) noexcept {
        if (!instruction.lhs.isRegister())
            return;

        if (!instruction.rhs.isRegister()) {
            m_usedRegisters.emplace(std::get<Register>(instruction.lhs), Usage{});
            return;
        }

        const auto lhs = std::get<Register>(instruction.lhs);
        const auto rhs = std::get<Register>(instruction.rhs);

        Usage *rhsUsage{nullptr};
        auto rhsIt = m_usedRegisters.find(rhs);
        if (rhsIt == std::end(m_usedRegisters)) {
            rhsUsage = &m_usedRegisters.emplace(rhs, Usage{}).first->second;
        } else {
            rhsUsage = &rhsIt->second;
        }

        auto it = m_usedRegisters.find(lhs);
        if (it == std::end(m_usedRegisters)) {
            auto &usage = m_usedRegisters.emplace(lhs, Usage{}).first->second;
            usage.dependent.push_back(rhsUsage);
        } else {
            if (std::find(std::begin(it->second.dependent), std::end(it->second.dependent), rhsUsage)
                    == std::end(it->second.dependent)) {
                it->second.dependent.push_back(rhsUsage);
            }
        }
    }

    void
    DeadCodeRemover::inspectMove(Instruction &instruction) noexcept {
        const auto lhs = std::get<Register>(instruction.lhs);
        if (instruction.rhs.isImmediateUnsigned() && m_usedRegisters.find(lhs) == std::end(m_usedRegisters)) {
            m_usedRegisters.emplace(lhs, Usage{});
            return;
        }

        if (!instruction.rhs.isRegister())
            return;

        const auto rhs = std::get<Register>(instruction.rhs);

        Usage *rhsUsage{nullptr};
        auto rhsIt = m_usedRegisters.find(rhs);
        if (rhsIt == std::end(m_usedRegisters)) {
            rhsUsage = &m_usedRegisters.emplace(rhs, Usage{}).first->second;
        } else {
            rhsUsage = &rhsIt->second;
        }

        auto it = m_usedRegisters.find(lhs);
        if (it == std::end(m_usedRegisters)) {
            auto &usage = m_usedRegisters.emplace(lhs, Usage{}).first->second;
            usage.dependent.push_back(rhsUsage);
        } else {
            if (std::find(std::begin(it->second.dependent), std::end(it->second.dependent), rhsUsage)
                == std::end(it->second.dependent)) {
                it->second.dependent.push_back(rhsUsage);
            }
        }
    }

    void
    DeadCodeRemover::inspectReturn(Instruction &instruction) noexcept {
        if (instruction.lhs.isRegister()) {
            const auto reg = std::get<Register>(instruction.lhs);
            auto it = m_usedRegisters.find(reg);
            if (it == std::end(m_usedRegisters)) {
                m_usedRegisters.emplace(reg, Usage{true});
            } else {
                it->second.definitelyUsed = true;
            }
        }
    }

    [[nodiscard]] inline bool
    isDependentlyUsed(const DeadCodeRemover::Usage *usage) noexcept {
        if (usage->definitelyUsed)
            return true;

        return std::any_of(std::cbegin(usage->dependent), std::end(usage->dependent), [] (const auto *depend) {
            return isDependentlyUsed(depend);
        });
    }

    void
    DeadCodeRemover::run() noexcept {
        for (auto &instruction : block().instructions) {
            switch (instruction.type) {
                case InstructionType::ADD:
                case InstructionType::DIVIDE:
                case InstructionType::MULTIPLY:
                case InstructionType::SUBTRACT:
                    inspectGeneral(instruction);
                    break;
                case InstructionType::MOVE:
                    inspectMove(instruction);
                    break;
                case InstructionType::RETURN:
                    inspectReturn(instruction);
                    break;
                default:
                    logger::warning("IntermediateOptimizer: unknown instruction type {}", toString(instruction.type));
                    break;
            }
        }

        logger::info("IO.DCR: count={}", std::size(m_usedRegisters));
        for (const auto &entry : m_usedRegisters) {
            if (!entry.second.definitelyUsed) {
                if (!isDependentlyUsed(&entry.second)) {
                    logger::info("IO.DCR: removing all references of r{}", entry.first.index);

                    for (auto &instruction : block().instructions) {
                        if (instruction.lhs.isRegister() && std::get<Register>(instruction.lhs) == entry.first) {

                        }
                    }
                    block().instructions.erase(
                            std::remove_if(std::begin(block().instructions),
                                           std::end(block().instructions),
                                           [&entry] (const Instruction &instruction) {
                                               return instruction.lhs.isRegister()
                                                   && std::get<Register>(instruction.lhs) == entry.first;
                                           }
                            ),
                            std::end(block().instructions)
                    );
                }
            }
        }
    }

} // namespace intermediate::optimization