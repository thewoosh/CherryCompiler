/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/IntermediateGenerator/FunctionBlock.hpp"

namespace intermediate::optimization {

    class SubOptimizer {
    public:
        [[nodiscard]] inline explicit
        SubOptimizer(FunctionBlock &block) noexcept
                : m_block(block) {
        }

        [[nodiscard]] inline FunctionBlock &
        block() noexcept {
            return m_block;
        }

        [[nodiscard]] inline constexpr bool
        hasSomethingChanged() const noexcept {
            return m_hasSomethingChanged;
        }

    protected:
        inline constexpr void
        markSomethingChanged() noexcept {
            m_hasSomethingChanged = true;
        }

    private:
        FunctionBlock &m_block;
        bool m_hasSomethingChanged{false};
    };

} // namespace intermediate::optimization
