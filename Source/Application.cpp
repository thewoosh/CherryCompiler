/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Application.hpp"

#include <functional>

#include "Source/Base/ExitCode.hpp"
#include "Source/CodeGenerator/CodeGenerator.hpp"
#include "Source/IntermediateGenerator/IntermediateGenerator.hpp"
#include "Source/IntermediateOptimizer/IntermediateOptimizer.hpp"
#include "Source/Lexer/Lexer.hpp"
#include "Source/Parser/Parser.hpp"

#ifdef __unix__
#   include <cstdlib> // for std::aligned_alloc
#   include <unistd.h> // for ::sysconf
#   include <sys/mman.h> // for ::
#endif

int
Application::executeFunction(const codegen::FunctionCodeBlock &block) noexcept {
#ifdef __unix__
    const auto pageSize = static_cast<std::size_t>(sysconf(_SC_PAGE_SIZE));
    auto bufferSize = std::size(block.code()) / pageSize;
    if (std::size(block.code()) % pageSize != 0)
        bufferSize += pageSize;

    auto *buffer = std::aligned_alloc(pageSize, bufferSize);
    if (buffer == nullptr) {
        logger::error("Application: failed to allocate memory");
        return base::ExitCode::UnspecifiedError;
    }

    std::copy(std::begin(block.code()), std::end(block.code()), reinterpret_cast<char8_t *>(buffer));

    if (mprotect(buffer, bufferSize, PROT_READ | PROT_EXEC) == -1) {
        logger::error("Application: failed to memprotect()");
        return base::ExitCode::UnspecifiedError;
    }

    std::string executablePath{"/dev/null"};
    std::vector<char *> arguments{
        executablePath.data(), nullptr
    };

    const std::function<int(int argc, char **argv)> function{reinterpret_cast<int (*)(int argc, char **argv)>(buffer)};

    int exitCode = function(static_cast<int>(std::size(arguments)), std::data(arguments));
    logger::info("Application: function returned exit code {}", exitCode);

    std::free(buffer);
    return base::ExitCode::Success;
#else
    logger::error("Application: cannot execute function on non-POSIX systems!");
    return base::ExitCode::UnspecifiedError;
#endif
}

int
Application::run() noexcept {
    lexer::Lexer lexer{std::string(m_fileName)};
    if (!lexer.run()) {
        logger::trace("Application: failed to invoke lexer");
        return base::ExitCode::LexerFailure;
    }

    parser::Parser parser{lexer.tokens()};
    if (!parser.run()) {
        logger::trace("Application: failed to invoke parser");
        logger::trace("Application: parser could make up the following tree:");
        parser.printAST();
        return base::ExitCode::ParserFailure;
    }

    parser.printAST();

    intermediate::IntermediateGenerator intermediateGenerator{&parser.rootNode()};
    if (!intermediateGenerator.run()) {
        logger::trace("Application: failed to invoke intermediate generator");
        return base::ExitCode::IntermediateGeneratorFailure;
    }

    intermediate::Optimizer intermediateOptimizer{intermediateGenerator.functionBlocks()};
    intermediateOptimizer.run();

    codegen::CodeGenerator codeGenerator{intermediateGenerator.functionBlocks()};
    if (!codeGenerator.run()) {
        logger::trace("Application: failed to invoke code generator");
        return base::ExitCode::CodeGeneratorFailure;
    }

    if (std::empty(codeGenerator.functionCodeBlocks()) || std::empty(codeGenerator.functionCodeBlocks().back().code())) {
        logger::info("Application: refraining from invoking the program since the main function is empty");
        return base::ExitCode::Success;
    }

    return executeFunction(codeGenerator.functionCodeBlocks().front());
}
