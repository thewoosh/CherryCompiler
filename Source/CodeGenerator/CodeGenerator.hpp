/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Base/CompilerFeatures.hpp"
#include "Source/CodeGenerator/FunctionCodeBlock.hpp"
#include "Source/IntermediateGenerator/FunctionBlock.hpp"

namespace codegen {

    class CodeGenerator {
    public:
        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR explicit
        CodeGenerator(const std::vector<intermediate::FunctionBlock> &intermediateBlocks) noexcept
                : m_intermediateBlocks(intermediateBlocks) {
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR std::vector<FunctionCodeBlock> &
        functionCodeBlocks() noexcept {
            return m_functionCodeBlocks;
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR const std::vector<FunctionCodeBlock> &
        functionCodeBlocks() const noexcept {
            return m_functionCodeBlocks;
        }

        [[nodiscard]] bool
        run() noexcept;

    private:
        const std::vector<intermediate::FunctionBlock> &m_intermediateBlocks;
        std::vector<FunctionCodeBlock> m_functionCodeBlocks{};
    };

} // namespace codegen
