/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/CodeGenerator/Modules/RegisterAllocation.hpp"

#include "Source/Base/Assert.hpp"

namespace codegen {

    void
    setRegisterRelation(FunctionContext &context, intermediate::Register intermediateRegister,
                        RegisterName registerName) noexcept {
        auto it = std::find_if(std::begin(context.registers), std::end(context.registers),
                               [intermediateRegister] (const RegisterRelation &relation) {
                                   return relation.intermediateRegister.index == intermediateRegister.index;
                               }
        );

        if (it == std::end(context.registers)) {
            context.registers.emplace_back(intermediateRegister, registerName);
        } else {
            it->codegenRegister = registerName;
        }
    }

    RegisterName
    findRegisterRelation(FunctionContext &context, intermediate::Register intermediateRegister) noexcept {
        auto it = std::find_if(std::begin(context.registers), std::end(context.registers),
                               [intermediateRegister] (const RegisterRelation &relation) {
                                   return relation.intermediateRegister.index == intermediateRegister.index;
                               }
        );

        if (it == std::end(context.registers))
            return RegisterName::INVALID_END;

        return it->codegenRegister;
    }

    RegisterName
    allocateRegister(FunctionContext &context) noexcept {
        CHERRY_ASSERT(static_cast<std::size_t>(RegisterName::INVALID_END) > context.registerIndex);
        return static_cast<RegisterName>(context.registerIndex++);
    }

    RegisterRelation &
    getOrAllocateRegister(FunctionContext &context, intermediate::Register intermediateRegister) noexcept {
        auto it = std::find_if(std::begin(context.registers), std::end(context.registers),
                               [intermediateRegister] (const RegisterRelation &relation) {
                                   return relation.intermediateRegister.index == intermediateRegister.index;
                               }
        );

        if (it != std::end(context.registers))
            return *it;

        const auto registerName = allocateRegister(context);
        return context.registers.emplace_back(intermediateRegister, registerName);
    }

} // namespace codegen
