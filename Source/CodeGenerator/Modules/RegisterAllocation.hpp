/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/CodeGenerator/FunctionContext.hpp"
#include "Source/IntermediateGenerator/Register.hpp"

namespace codegen {

    void
    setRegisterRelation(FunctionContext &context, intermediate::Register intermediateRegister,
                        RegisterName registerName) noexcept;

    [[nodiscard]] RegisterName
    findRegisterRelation(FunctionContext &context, intermediate::Register intermediateRegister) noexcept;

    [[nodiscard]] RegisterName
    allocateRegister(FunctionContext &context) noexcept;

    [[nodiscard]] RegisterRelation &
    getOrAllocateRegister(FunctionContext &context, intermediate::Register intermediateRegister) noexcept;

} // namespace codegen
