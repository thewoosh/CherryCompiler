/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace codegen {

    enum class RegisterName {
        ACCUMULATOR,
        COUNTER,
        DATA,
        BASE,
        STACK_POINTER,
        BASE_POINTER,
        SOURCE_INDEX,
        DESTINATION_INDEX,

        R8,
        R9,
        R10,
        R11,
        R12,
        R13,
        R14,
        R15,


        INVALID_END
    };

    struct VariableState {
        std::string_view name;

        bool inRegister{};
        union {
            RegisterName registerName;
        };
    };

} // namespace codegen
