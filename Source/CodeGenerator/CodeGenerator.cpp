/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/CodeGenerator/CodeGenerator.hpp"

#include <limits>

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/CodeGenerator/FunctionContext.hpp"
#include "Source/CodeGenerator/Modules/RegisterAllocation.hpp"

namespace codegen {

    [[nodiscard]] bool
    setRegister(FunctionContext &context, RegisterName registerName, std::uint64_t value) noexcept {
        const auto index = static_cast<char8_t>(registerName);

        if (value == 0) {
            if (registerName < RegisterName::R8) {
                context.block.push(0x31); // xor
                context.block.push(0xc0 + index * 9); // <reg>, <reg>
            } else if (registerName <= RegisterName::R15) {
                context.block.push(0x47);
                context.block.push(0x31);
                context.block.push(0xc0 + (index - static_cast<char8_t>(RegisterName::R8)) * 9); // <reg>, <reg>
            } else {
                CHERRY_ASSERT_NOT_REACHED_OR_RETURN(false);
            }
            return true;
        }

        if (value <= std::numeric_limits<std::uint32_t>::max()) {
            if (registerName < RegisterName::R8) {
                context.block.push(0xb8 + index);
            } else if (registerName <= RegisterName::R15) {
                context.block.push(0x49);
                context.block.push(0xc7);
                context.block.push(0xc0 + index - static_cast<char8_t>(RegisterName::R8));
            } else {
                CHERRY_ASSERT_NOT_REACHED_OR_RETURN(false);
            }

            const auto *arr = reinterpret_cast<const char8_t *>(&value);
            context.block.push(arr[0]);
            context.block.push(arr[1]);
            context.block.push(arr[2]);
            context.block.push(arr[3]);
            return true;
        }

        // TODO
        return false;
    }

    void
    moveRegisterToReturnValue(FunctionContext &context, RegisterName registerName) noexcept {
        if (registerName == RegisterName::ACCUMULATOR) {
            // `mov eax, eax` has no effect
            return;
        }

        if (registerName >= RegisterName::R8)
            context.block.push(0x44);

        context.block.push(0x89); // mov eax,

        auto index = static_cast<char8_t>(registerName);
        if (registerName >= RegisterName::R8 && registerName < RegisterName::R15) {
            index -= static_cast<char8_t>(RegisterName::R8);
            return;
        }

        context.block.push(0xc0 + index * 8);
    }

    inline void
    initializeParameter(FunctionContext &context,
                        std::size_t index,
                        const std::pair<intermediate::Register, const parser::ParameterDeclaration *> &parameter) {
        context.parameterRelations.emplace_back(parameter.first, parameter.second);

        constexpr const std::array registerList{
            RegisterName::DESTINATION_INDEX,
            RegisterName::SOURCE_INDEX,
            RegisterName::DATA,
            RegisterName::COUNTER,
            RegisterName::R8,
            RegisterName::R9
        };

        context.registers.emplace_back(
                parameter.first,
                registerList[static_cast<std::size_t>(index)]
        );
    }

    [[nodiscard]] inline bool
    processAddInstruction(FunctionContext &context, const intermediate::Instruction &instruction) noexcept {
        CHERRY_ASSERT(instruction.lhs.isRegister());
        CHERRY_ASSERT(!instruction.rhs.isNull());

        if (instruction.rhs.isRegister()) {
            auto lhs = getOrAllocateRegister(context, std::get<intermediate::Register>(instruction.lhs));
            auto rhs = findRegisterRelation(context, std::get<intermediate::Register>(instruction.rhs));

            context.block.push(0x01); // mov
            context.block.push(
                    0xc0 +
                    static_cast<char8_t>(lhs.codegenRegister) +
                    static_cast<char8_t>(rhs) * 8
            );
            return true;
        }

        if (instruction.rhs.isImmediateUnsigned()) {
            auto lhs = getOrAllocateRegister(context, std::get<intermediate::Register>(instruction.lhs));
            const auto value = std::get<std::uint64_t>(instruction.rhs);

            if (value < 0x80) {
                context.block.push(0x83);
                context.block.push(0xc0 + static_cast<char8_t>(lhs.codegenRegister));
                context.block.push(static_cast<char8_t>(value));
            } else {
                if (lhs.codegenRegister == RegisterName::ACCUMULATOR) {
                    context.block.push(0x05);
                } else {
                    context.block.push(0x83);
                    context.block.push(0xc0 + static_cast<char8_t>(lhs.codegenRegister));
                }

                const auto *arr = reinterpret_cast<const char8_t *>(&value);
                context.block.push(arr[0]);
                context.block.push(arr[1]);
                context.block.push(arr[2]);
                context.block.push(arr[3]);
            }

            return true;
        }

        CHERRY_ASSERT_NOT_REACHED_OR_RETURN(false);
    }

    [[nodiscard]] inline bool
    processMoveInstruction(FunctionContext &context, const intermediate::Instruction &instruction) noexcept {
        CHERRY_ASSERT(instruction.lhs.isRegister());
        CHERRY_ASSERT(!instruction.rhs.isNull());

        if (instruction.rhs.isImmediateUnsigned()) {
            const auto registerName = allocateRegister(context);
            setRegisterRelation(context, std::get<intermediate::Register>(instruction.lhs), registerName);
            return setRegister(context, registerName, std::get<std::uint64_t>(instruction.rhs));
        }

        if (instruction.rhs.isRegister()) {
            auto lhs = getOrAllocateRegister(context, std::get<intermediate::Register>(instruction.lhs));
            auto rhs = findRegisterRelation(context, std::get<intermediate::Register>(instruction.rhs));

            logger::trace("mov reg,reg {} {}", lhs.codegenRegister, rhs);

            context.block.push(0x89); // mov
            context.block.push(
                    0xc0 +
                    static_cast<char8_t>(lhs.codegenRegister) +
                    static_cast<char8_t>(rhs) * 8
            );
            return true;
        }

        CHERRY_ASSERT_NOT_REACHED_OR_RETURN(false);
    }

    [[nodiscard]] inline bool
    processReturnInstruction(FunctionContext &context, const intermediate::Instruction &instruction) noexcept {
        CHERRY_ASSERT(instruction.rhs.isNull());

        if (!instruction.lhs.isNull()) {
            if (instruction.lhs.isImmediateUnsigned()) {
                if (!setRegister(context, RegisterName::ACCUMULATOR, std::get<std::uint64_t>(instruction.lhs)))
                    return false;
            } else if (instruction.lhs.isRegister()) {
                auto intermediateRegister = findRegisterRelation(context, std::get<intermediate::Register>(instruction.lhs));
                moveRegisterToReturnValue(context, intermediateRegister);
            } else {
                CHERRY_ASSERT_NOT_REACHED_OR_RETURN(false);
            }
        }

        context.block.push(0xc3); // ret
        std::putchar('\n');
        return true;
    }

    [[nodiscard]] inline bool
    processSubInstruction(FunctionContext &context, const intermediate::Instruction &instruction) noexcept {
        CHERRY_ASSERT(instruction.lhs.isRegister());
        CHERRY_ASSERT(!instruction.rhs.isNull());

        if (instruction.rhs.isRegister()) {
            auto lhs = getOrAllocateRegister(context, std::get<intermediate::Register>(instruction.lhs));
            auto rhs = findRegisterRelation(context, std::get<intermediate::Register>(instruction.rhs));

            context.block.push(0x01); // mov
            context.block.push(
                    0xc0 +
                    static_cast<char8_t>(lhs.codegenRegister) +
                    static_cast<char8_t>(rhs) * 8
            );
            return true;
        }

        if (instruction.rhs.isImmediateUnsigned()) {
            auto lhs = getOrAllocateRegister(context, std::get<intermediate::Register>(instruction.lhs));
            const auto value = std::get<std::uint64_t>(instruction.rhs);

            if (value < 0x80) {
                context.block.push(0x83);
                context.block.push(0xe8 + static_cast<char8_t>(lhs.codegenRegister));
                context.block.push(static_cast<char8_t>(value));
            } else {
                if (lhs.codegenRegister == RegisterName::ACCUMULATOR) {
                    context.block.push(0x2d);
                } else {
                    context.block.push(0x81); // sub
                    context.block.push(0xe8 + static_cast<char8_t>(lhs.codegenRegister));
                }

                const auto *arr = reinterpret_cast<const char8_t *>(&value);
                context.block.push(arr[0]);
                context.block.push(arr[1]);
                context.block.push(arr[2]);
                context.block.push(arr[3]);
            }

            return true;
        }

        CHERRY_ASSERT_NOT_REACHED_OR_RETURN(false);
    }

    [[nodiscard]] inline bool
    processInstruction(FunctionContext &context, const intermediate::Instruction &instruction) noexcept {
        std::putchar('\n');
        logger::error("CG: instruction type={} lhs={}, rhs={}",
                      intermediate::toString(instruction.type), instruction.lhs.toString(), instruction.rhs.toString());
        switch (instruction.type) {
            case intermediate::InstructionType::ADD:
                return processAddInstruction(context, instruction);
            case intermediate::InstructionType::MOVE:
                return processMoveInstruction(context, instruction);
            case intermediate::InstructionType::RETURN:
                return processReturnInstruction(context, instruction);
            case intermediate::InstructionType::SUBTRACT:
                return processSubInstruction(context, instruction);
            default:
                logger::error("CG: unknown instruction type={} lhs={}, rhs={}",
                              intermediate::toString(instruction.type), instruction.lhs.toString(),
                              instruction.rhs.toString());
                return false;
        }
    }

    bool
    CodeGenerator::run() noexcept {
        for (const auto &intermediateBlock : m_intermediateBlocks) {
            FunctionContext context{m_functionCodeBlocks.emplace_back(std::string(intermediateBlock.name))};

            for (std::size_t i = 0; i < std::size(intermediateBlock.parameters); ++i) {
                initializeParameter(context, i, intermediateBlock.parameters[i]);
            }

            for (const auto &instruction : intermediateBlock.instructions) {
                if (!processInstruction(context, instruction))
                    return false;
            }

            logger::info("CG: resulting bytecode:");
            for (std::size_t i = 0; i < context.block.code().size(); ++i) {
                if (i != 0 && i % 8 == 0)
                    std::putchar('\n');
                std::printf("%hhx ", static_cast<unsigned char>(context.block.code()[i]));
            }
            std::putchar('\n');

            for (const auto &relation : context.registers) {
                constexpr const std::array arr{
                    "eax", "ecx", "edx", "ebx",
                    "esp", "ebp", "esi", "edi",
                    "r8", "r9", "r10", "r11",
                    "r12", "r13", "r14", "r15"
                };
                const auto index = static_cast<std::size_t>(relation.codegenRegister);
                CHERRY_ASSERT(index < std::size(arr));
                logger::debug("CG: Relation {} = {}", relation.intermediateRegister.index, arr[index]);
            }
        }

        return true;
    }

} // namespace codegen
