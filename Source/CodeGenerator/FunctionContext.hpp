/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <algorithm>
#include <vector>

#include "Source/Base/CompilerFeatures.hpp"
#include "Source/CodeGenerator/FunctionCodeBlock.hpp"
#include "Source/CodeGenerator/RegisterRelation.hpp"
#include "Source/CodeGenerator/VariableState.hpp"
#include "Source/Parser/Types/ParameterDeclaration.hpp"

namespace codegen {

    struct FunctionContext {
        FunctionCodeBlock &block;
        std::vector<VariableState> variables{};
        std::vector<RegisterRelation> registers{};

        std::vector<std::pair<intermediate::Register, const parser::ParameterDeclaration *>> parameterRelations{};

        std::size_t registerIndex{0};

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR VariableState *
        findVariable(std::string_view name) noexcept {
            return &(*std::find_if(std::begin(variables),
                                   std::end(variables),
                                   [name] (const VariableState &state) {
                                       return state.name == name;
                                   }));
        }
    };

} // namespace codegen
