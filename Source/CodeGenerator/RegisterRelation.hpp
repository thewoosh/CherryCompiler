/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/CodeGenerator/VariableState.hpp"
#include "Source/IntermediateGenerator/Register.hpp"

namespace codegen {

    struct RegisterRelation {
        intermediate::Register intermediateRegister;
        RegisterName codegenRegister;

        [[nodiscard]] inline constexpr
        RegisterRelation(intermediate::Register intermediateRegister, RegisterName codegenRegister) noexcept
                : intermediateRegister(intermediateRegister)
                , codegenRegister(codegenRegister) {
        }
    };

} // namespace codegen
