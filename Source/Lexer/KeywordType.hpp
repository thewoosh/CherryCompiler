/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <array>
#include <string_view>

namespace lexer {

    enum class KeywordType {
        AUTO, BREAK, CASE, CHAR, CONST, CONTINUE, DEFAULT, DO,
        DOUBLE, ELSE, ENUM, EXTERN, FLOAT, FOR, GOTO, IF,
        INLINE, INT, LONG, REGISTER, RESTRICT, RETURN, SHORT,
        SIGNED, SIZEOF, STATIC, STRUCT, SWITCH, TYPEDEF, UNION,
        UNSIGNED, VOID, VOLATILE, WHILE,

        INVALID,
    };

    constexpr const std::array<std::string_view, 34> g_keywordList{
            "auto", "break", "case", "char", "const", "continue", "default", "do",
            "double", "else", "enum", "extern", "float", "for", "goto", "if",
            "inline", "int", "long", "register", "restrict", "return", "short",
            "signed", "sizeof", "static", "struct", "switch", "typedef",
            "union", "unsigned", "void", "volatile", "while"
    };

    static_assert(std::size(g_keywordList) == static_cast<std::size_t>(KeywordType::INVALID));

    [[nodiscard]] inline constexpr KeywordType
    convertStringToKeyword(std::string_view string) noexcept {
        const auto it = std::find(std::cbegin(g_keywordList), std::cend(g_keywordList), string);

        if (it == std::cend(g_keywordList)) {
            return KeywordType::INVALID;
        }

        return static_cast<KeywordType>(std::distance(std::cbegin(g_keywordList), it));
    }

} // namespace lexer
