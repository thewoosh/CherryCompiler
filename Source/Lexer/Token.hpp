/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <variant>

#include <cstdint>

#include "Source/Lexer/KeywordType.hpp"
#include "Source/Lexer/PunctuatorType.hpp"
#include "Source/Lexer/TokenType.hpp"

namespace lexer {

    struct Token
            : public std::variant<std::string, KeywordType, PunctuatorType, std::uint64_t> {
        template <typename Type>
        [[nodiscard]] inline constexpr
        Token(TokenType type, Type &&data) noexcept
                : variant(std::forward<Type>(data))
                , m_type(type) {
        }

        [[nodiscard]] std::string
        toString() const noexcept;

        [[nodiscard]] inline constexpr TokenType
        type() const noexcept {
            return m_type;
        }

    private:
        TokenType m_type;
    };

} // namespace lexer
