/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace lexer {

    enum class TokenType {
        IDENTIFIER,
        KEYWORD,
        NUMBER_DECIMAL,
        NUMBER_UNSIGNED,
        PUNCTUATOR,
        STRING,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(TokenType tokenType) noexcept {
        switch (tokenType) {
            case TokenType::IDENTIFIER:         return "identifier";
            case TokenType::KEYWORD:            return "keyword";
            case TokenType::NUMBER_DECIMAL:     return "number-decimal";
            case TokenType::NUMBER_UNSIGNED:    return "number-unsigned";
            case TokenType::PUNCTUATOR:         return "punctuator";
            case TokenType::STRING:             return "string";
            default:                            return "(invalid)";
        }
    }

} // namespace lexer
