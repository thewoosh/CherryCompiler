/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace lexer {

    enum class PunctuatorType {
        INVALID,

        ADDITION,                                               // +
        DIVISION,                                               // /
        MULTIPLICATION,                                         // *
        SUBTRACTION,                                            // -

        LEFT_CURLY_BRACKET,                                     // {
        LEFT_PARENTHESIS,                                       // (
        LEFT_SQUARE_BRACKET,                                    // [
        RIGHT_CURLY_BRACKET,                                    // }
        RIGHT_PARENTHESIS,                                      // )
        RIGHT_SQUARE_BRACKET,                                   // ]

        ASSIGNMENT,                                             // =

        COMMA,                                                  // ,
        SEMICOLON,                                              // ;
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(PunctuatorType tokenType) noexcept {
        switch (tokenType) {
            case PunctuatorType::ADDITION:              return "addition( + )";
            case PunctuatorType::DIVISION:              return "division( / )";
            case PunctuatorType::MULTIPLICATION:        return "multiplication( * )";
            case PunctuatorType::SUBTRACTION:           return "subtraction( - )";

            case PunctuatorType::LEFT_CURLY_BRACKET:    return "left-curly-bracket( ] )";
            case PunctuatorType::LEFT_PARENTHESIS:      return "left-parenthesis( ( )";
            case PunctuatorType::LEFT_SQUARE_BRACKET:   return "left-square-bracket( [ )";
            case PunctuatorType::RIGHT_CURLY_BRACKET:   return "right-curly-bracket( } )";
            case PunctuatorType::RIGHT_PARENTHESIS:     return "right-parenthesis( ) )";
            case PunctuatorType::RIGHT_SQUARE_BRACKET:  return "right-square-bracket( ] )";

            case PunctuatorType::ASSIGNMENT:            return "assignment( = )";

            case PunctuatorType::COMMA:                 return "comma( , )";
            case PunctuatorType::SEMICOLON:             return "semicolon( ; )";
            default:                                    return "(invalid)";
        }
    }

} // namespace lexer
