/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>

#include "Source/Parser/Expressions/Expression.hpp"
#include "Source/Parser/Node.hpp"

namespace parser {

    struct ReturnStatementNode
            : public Node {

        [[nodiscard]] inline explicit
        ReturnStatementNode(std::unique_ptr<Expression> &&expression) noexcept
                : Node(NodeType::RETURN_STATEMENT)
                , m_expression(std::move(expression)) {
        }

        [[nodiscard]] inline constexpr const Expression *
        expression() const noexcept {
            return m_expression.get();
        }

    private:
        std::unique_ptr<Expression> m_expression;
    };

} // namespace parser
