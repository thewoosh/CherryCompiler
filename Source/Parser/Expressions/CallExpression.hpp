/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>

#include "Source/Parser/Expressions/Expression.hpp"

namespace parser {

    struct CallExpression
            : public Expression {

        [[nodiscard]] inline
        CallExpression(std::unique_ptr<Expression> &&callee,
                       std::vector<std::unique_ptr<Expression> >&&parameters) noexcept
                : Expression(ExpressionType::FUNCTION_CALL)
                , m_callee(std::move(callee))
                , m_parameters(std::move(parameters)) {
        }

        [[nodiscard]] inline const Expression *
        callee() const noexcept {
            return m_callee.get();
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Expression>> &
        parameters() const noexcept {
            return m_parameters;
        }

    private:
        std::unique_ptr<Expression> m_callee;
        std::vector<std::unique_ptr<Expression>> m_parameters;
    };

} // namespace parser
