/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>

#include "Source/Parser/Expressions/Expression.hpp"

namespace parser {

    struct BiExpression
            : public Expression {

        [[nodiscard]] inline
        BiExpression(ExpressionType type, std::unique_ptr<Expression> &&lhs, std::unique_ptr<Expression> &&rhs) noexcept
                : Expression(type)
                , m_lhs(std::move(lhs))
                , m_rhs(std::move(rhs)) {
        }

        [[nodiscard]] inline const Expression *
        lhs() const noexcept {
            return m_lhs.get();
        }

        [[nodiscard]] inline const Expression *
        rhs() const noexcept {
            return m_rhs.get();
        }

    private:
        std::unique_ptr<Expression> m_lhs;
        std::unique_ptr<Expression> m_rhs;
    };

} // namespace parser
