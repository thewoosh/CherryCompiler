/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace parser {

    enum class ExpressionType {
        ADDITIVE_ADD,
        ADDITIVE_SUBTRACT,
        EMPTY,
        FUNCTION_CALL,
        MULTIPLICATIVE_DIVIDE,
        MULTIPLICATIVE_MULTIPLY,
        PRIMARY_DOUBLE,
        PRIMARY_IDENTIFIER,
        PRIMARY_STRING_LITERAL,
        PRIMARY_UNSIGNED,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(ExpressionType expressionType) noexcept {
        switch (expressionType) {
            case ExpressionType::ADDITIVE_ADD:              return "additive-add";
            case ExpressionType::ADDITIVE_SUBTRACT:         return "additive-subtract";
            case ExpressionType::EMPTY:                     return "empty";
            case ExpressionType::FUNCTION_CALL:             return "function-call";
            case ExpressionType::MULTIPLICATIVE_DIVIDE:     return "multiplicative-divide";
            case ExpressionType::MULTIPLICATIVE_MULTIPLY:   return "multiplicative-multiply";
            case ExpressionType::PRIMARY_DOUBLE:            return "primary-double";
            case ExpressionType::PRIMARY_IDENTIFIER:        return "primary-identifier";
            case ExpressionType::PRIMARY_STRING_LITERAL:    return "primary-string-literal";
            case ExpressionType::PRIMARY_UNSIGNED:          return "primary-unsigned";
            default:                                        return "(invalid)";
        }
    }

} // namespace parser
