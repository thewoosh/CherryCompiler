/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include <cstdint>

#include "Source/Parser/Expressions/Expression.hpp"

namespace parser {

    template <typename Contained>
    struct PrimaryExpression
            : public Expression {

        [[nodiscard]] inline constexpr explicit
        PrimaryExpression(double &&number) noexcept
                : Expression(ExpressionType::PRIMARY_DOUBLE)
                , m_data(number) {
        }

        [[nodiscard]] inline explicit
        PrimaryExpression(ExpressionType type, std::string &&identifier) noexcept
                : Expression(type)
                , m_data(std::move(identifier)) {
        }

        [[nodiscard]] inline explicit
        PrimaryExpression(ExpressionType type, const std::string &identifier) noexcept
                : Expression(type)
                , m_data(identifier) {
        }

        [[nodiscard]] inline constexpr explicit
        PrimaryExpression(std::uint64_t unsignedInteger) noexcept
                : Expression(ExpressionType::PRIMARY_UNSIGNED)
                , m_data(unsignedInteger) {
        }

        [[nodiscard]] inline constexpr Contained &
        data() noexcept {
            return m_data;
        }

        [[nodiscard]] inline constexpr const Contained &
        data() const noexcept {
            return m_data;
        }

    private:
        Contained m_data;
    };

} // namespace parser
