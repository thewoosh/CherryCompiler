/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Parser/Expressions/ExpressionType.hpp"

namespace parser {

    struct Expression {
        [[nodiscard]] inline constexpr explicit
        Expression(ExpressionType expressionType) noexcept
                : m_type(expressionType) {
        }

        [[nodiscard]] inline constexpr ExpressionType
        type() const noexcept {
            return m_type;
        }

    private:
        ExpressionType m_type;
    };

} // namespace parser
