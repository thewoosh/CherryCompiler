/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Parser/Parser.hpp"

#include "Source/Parser/FunctionDefinitionNode.hpp"
#include "Source/Parser/Nodes/VariableDeclarationNode.hpp"
#include "Source/Parser/ReturnStatementNode.hpp"

namespace parser {

    static void
    printParentNode(const std::string &prefix, std::size_t depth, const Node *nodePtr) noexcept {
        const auto *node = static_cast<const ParentNode *>(nodePtr);
        logger::debug("{}Child count: {}", prefix, std::size(node->children()));
        for (const auto &child : node->children()) {
            Parser::printAST(child.get(), depth + 1);
        }
    }

    static void
    printDeclarationSpecifiers(const std::string &prefix, const DeclarationSpecifiers &declarationSpecifiers) noexcept {
        logger::debug("{}TypeQualifiers: {}", prefix, std::size(declarationSpecifiers.qualifiers));
        for (const auto &qualifier : declarationSpecifiers.qualifiers) {
            logger::debug("{}    TypeQualifier: {}", prefix, toString(qualifier));
        }

        logger::debug("{}TypeSpecifiers: {}", prefix, std::size(declarationSpecifiers.qualifiers));
        for (const auto &specifier : declarationSpecifiers.specifiers) {
            logger::debug("{}    Type: {}", prefix, toString(specifier.type));
        }
    }

    static void
    printDeclarator(const std::string &prefix, const Declarator &declarator) noexcept {
        logger::debug("{}Pointers: {}", prefix, std::size(declarator.pointers));
        for (const auto &pointer : declarator.pointers)
            logger::debug("{}    PointerTypeQualifiers: {}", prefix, std::size(pointer.typeQualifierList));
        logger::debug("{}Identifier: {}", prefix, declarator.identifier);
    }

    static void
    printFunctionDefinition(const std::string &prefix, std::size_t depth, const Node *nodePtr) noexcept {
        const auto *node = static_cast<const FunctionDefinitionNode *>(nodePtr);
        logger::debug("{}Identifier: \"{}\"", prefix, node->identifier());
        logger::debug("{}ParameterCount: \"{}\"", prefix, std::size(node->parameterDeclarations()));
        for (const auto &parameter : node->parameterDeclarations()) {
            printDeclarationSpecifiers(prefix + "    ", parameter.declarationSpecifiers);
            printDeclarator(prefix + "    ", parameter.declarator);
        }

        printParentNode(prefix, depth, node);
    }

    void
    Parser::printAST(const Node *node, std::size_t depth) noexcept {
        const std::string prefix(depth * 4, ' ');

        logger::debug("{}Node{{type={}}}", prefix, toString(node->type()));
        switch (node->type()) {
            case NodeType::ROOT:
            case NodeType::COMPOUND_STATEMENT:
                printParentNode(prefix, depth, node);
                break;
            case NodeType::FUNCTION_DEFINITION:
                printFunctionDefinition(prefix, depth, node);
                break;
            case NodeType::RETURN_STATEMENT: {
                const auto *statement = static_cast<const ReturnStatementNode *>(node);
                if (statement->expression() == nullptr)
                    logger::debug("{}    Expression: void", prefix);
                else
                    printExpression(statement->expression(), depth + 1);
            } break;
            case NodeType::VARIABLE_DECLARATION: {
                const auto *declaration = static_cast<const VariableDeclarationNode *>(node);
                logger::debug("{}    Identifier: {}", prefix, declaration->identifier());
                if (declaration->expression() == nullptr)
                    logger::debug("{}    Assignment Expression: none", prefix);
                else
                    printExpression(declaration->expression(), depth + 1);
            } break;
            default:
                break;
        }
    }

} // namespace parser
