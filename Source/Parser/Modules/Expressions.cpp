/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Parser/Parser.hpp"

#include "Source/Parser/Expressions/BiExpression.hpp"
#include "Source/Parser/Expressions/CallExpression.hpp"
#include "Source/Parser/Expressions/PrimaryExpression.hpp"

namespace parser {

    using Iterator = std::vector<lexer::Token>::const_iterator;

    std::unique_ptr<Expression>
    consumeAssignmentExpression(Iterator &) noexcept;

    [[nodiscard]] inline bool
    consumePunctuator(Iterator &iterator, lexer::PunctuatorType punctuatorType) noexcept {
        if (iterator->type() != lexer::TokenType::PUNCTUATOR)
            return false;
        return std::get<lexer::PunctuatorType>(*iterator) == punctuatorType;
    }

    std::unique_ptr<Expression>
    consumePrimaryExpression(Iterator &iterator) noexcept {
        if (iterator->type() == lexer::TokenType::NUMBER_UNSIGNED) {
            return std::make_unique<PrimaryExpression<std::uint64_t>>(std::get<std::uint64_t>(*iterator++));
        }

        if (iterator->type() == lexer::TokenType::IDENTIFIER) {
            return std::make_unique<PrimaryExpression<std::string>>(ExpressionType::PRIMARY_IDENTIFIER, std::get<std::string>(*iterator++));
        }

        if (iterator->type() == lexer::TokenType::STRING) {
            return std::make_unique<PrimaryExpression<std::string>>(ExpressionType::PRIMARY_STRING_LITERAL, std::get<std::string>(*iterator++));
        }

        logger::error("Parser: token isn't primary expression: {}", iterator->toString());
        return nullptr;
    }

    [[nodiscard]] inline std::unique_ptr<Expression>
    consumePostfixExpression(Iterator &iterator) noexcept {
        auto expression = consumePrimaryExpression(iterator);
        if (expression == nullptr)
            return nullptr;

        while (true) {
            if (iterator->type() != lexer::TokenType::PUNCTUATOR
                    || std::get<lexer::PunctuatorType>(*iterator) != lexer::PunctuatorType::LEFT_PARENTHESIS) {
                return expression;
            }

            ++iterator;

            std::vector<std::unique_ptr<Expression>> parameters{};

            while (true) {
                auto param = consumeAssignmentExpression(iterator);
                if (param == nullptr)
                    return nullptr;

                parameters.push_back(std::move(param));


                if (iterator->type() != lexer::TokenType::PUNCTUATOR) {
                    logger::info("Parser: unexpected token in expression list: {}", iterator->toString());
                    return nullptr;
                }


                const auto punctuator = std::get<lexer::PunctuatorType>(*iterator);
                if (punctuator == lexer::PunctuatorType::RIGHT_PARENTHESIS) {
                    ++iterator;
                    break;
                }

                if (punctuator == lexer::PunctuatorType::COMMA) {
                    ++iterator;
                    continue;
                }

                logger::info("Parser: unexpected token in expression list: {}", iterator->toString());
                return nullptr;
            }

            expression = std::make_unique<CallExpression>(std::move(expression), std::move(parameters));
        }
    }

    [[nodiscard]] inline std::unique_ptr<Expression>
    consumeUnaryExpression(Iterator &iterator) noexcept {
        return consumePostfixExpression(iterator);
    }

    [[nodiscard]] inline std::unique_ptr<Expression>
    consumeCastExpression(Iterator &iterator) noexcept {
        return consumeUnaryExpression(iterator);
    }

    std::unique_ptr<Expression>
    consumeMultiplicativeExpression(Iterator &iterator) noexcept {
        auto expr = consumeCastExpression(iterator);
        if (!expr)
            return nullptr;

        while (true) {
            if (iterator->type() != lexer::TokenType::PUNCTUATOR)
                return expr;

            ExpressionType type;
            const auto punctuator = std::get<lexer::PunctuatorType>(*iterator);
            if (punctuator == lexer::PunctuatorType::MULTIPLICATION) {
                type = ExpressionType::MULTIPLICATIVE_MULTIPLY;
            } else if (punctuator == lexer::PunctuatorType::DIVISION) {
                type = ExpressionType::MULTIPLICATIVE_DIVIDE;
            } else {
                return expr;
            }

            // consume operator * or /
            ++iterator;

            auto rhs = consumeCastExpression(iterator);
            if (!rhs)
                return nullptr;

            expr = std::make_unique<BiExpression>(type, std::move(expr), std::move(rhs));
        }
    }

    std::unique_ptr<Expression>
    consumeAdditiveExpression(Iterator &iterator) noexcept {
        auto expr = consumeMultiplicativeExpression(iterator);
        if (!expr)
            return nullptr;

        while (true) {
            if (iterator->type() != lexer::TokenType::PUNCTUATOR)
                return expr;

            ExpressionType type;
            const auto punctuator = std::get<lexer::PunctuatorType>(*iterator);
            if (punctuator == lexer::PunctuatorType::ADDITION) {
                type = ExpressionType::ADDITIVE_ADD;
            } else if (punctuator == lexer::PunctuatorType::SUBTRACTION) {
                type = ExpressionType::ADDITIVE_SUBTRACT;
            } else {
                return expr;
            }

            // consume operator + or -
            ++iterator;

            auto rhs = consumeMultiplicativeExpression(iterator);
            if (!rhs)
                return nullptr;

            expr = std::make_unique<BiExpression>(type, std::move(expr), std::move(rhs));
        }
    }

    std::unique_ptr<Expression>
    consumeAssignmentExpression(Iterator &iterator) noexcept {
        return consumeAdditiveExpression(iterator);
    }

    std::unique_ptr<Expression>
    Parser::consumeExpression() noexcept {
        return consumeAssignmentExpression(m_it);
    }

} // namespace parser
