/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Parser/Parser.hpp"

#include <utility> // for std::forward

#include "Source/Base/Logger.hpp"

namespace parser {

    [[nodiscard]] inline std::pair<bool, TypeQualifier>
    consumeTypeQualifier(std::vector<lexer::Token>::const_iterator &iterator) noexcept {
        TypeQualifier qualifier{};

        if (iterator->type() != lexer::TokenType::KEYWORD)
            return {false, qualifier};

        switch (std::get<lexer::KeywordType>(*iterator)) {
            case lexer::KeywordType::CONST:
                qualifier = TypeQualifier::CONST;
                break;
            case lexer::KeywordType::RESTRICT:
                qualifier = TypeQualifier::RESTRICT;
                break;
//            case lexer::KeywordType::US_ATOMIC:
//                qualifier = TypeQualifier::US_ATOMIC;
//                break;
            case lexer::KeywordType::VOLATILE:
                qualifier = TypeQualifier::VOLATILE;
                break;
            default:
                return {false, qualifier};
        }

        ++iterator;
        return {true, qualifier};
    }

    [[nodiscard]] inline std::pair<bool, TypeSpecifier>
    consumeTypeSpecifier(std::vector<lexer::Token>::const_iterator &iterator) noexcept {
        TypeSpecifier specifier{};

        if (iterator->type() != lexer::TokenType::KEYWORD)
            return {false, specifier};

        switch (std::get<lexer::KeywordType>(*iterator)) {
            case lexer::KeywordType::VOID:
                specifier.type = TypeSpecifier::Type::VOID;
                break;
            case lexer::KeywordType::CHAR:
                specifier.type = TypeSpecifier::Type::CHAR;
                break;
            case lexer::KeywordType::SHORT:
                specifier.type = TypeSpecifier::Type::SHORT;
                break;
            case lexer::KeywordType::INT:
                specifier.type = TypeSpecifier::Type::INT;
                break;
            case lexer::KeywordType::LONG:
                specifier.type = TypeSpecifier::Type::LONG;
                break;
            case lexer::KeywordType::FLOAT:
                specifier.type = TypeSpecifier::Type::FLOAT;
                break;
            case lexer::KeywordType::DOUBLE:
                specifier.type = TypeSpecifier::Type::DOUBLE;
                break;
            case lexer::KeywordType::SIGNED:
                specifier.type = TypeSpecifier::Type::SIGNED;
                break;
            case lexer::KeywordType::UNSIGNED:
                specifier.type = TypeSpecifier::Type::UNSIGNED;
                break;
//            case lexer::KeywordType::US_BOOL,
//            case lexer::KeywordType::US_COMPLEX,
            default:
                return {false, specifier};
        }

        ++iterator;
        return {true, specifier};
    }


    DeclarationSpecifiers
    Parser::consumeDeclarationSpecifiers() noexcept {
        DeclarationSpecifiers declarationSpecifiers;

        while (m_it != std::end(m_tokens)) {
            if (const auto qualifier = consumeTypeQualifier(m_it); qualifier.first) {
                declarationSpecifiers.qualifiers.push_back(qualifier.second);
                continue;
            }

            if (const auto specifier = consumeTypeSpecifier(m_it); specifier.first) {
                declarationSpecifiers.specifiers.push_back(specifier.second);
                continue;
            }

            return declarationSpecifiers;
        }

        return declarationSpecifiers;
    }

    Declarator
    Parser::consumeDeclarator() noexcept {
        Declarator declarator;

        while (m_it != std::cend(m_tokens)) {
            if (m_it->type() != lexer::TokenType::PUNCTUATOR)
                break;

            if (std::get<lexer::PunctuatorType>(*m_it) != lexer::PunctuatorType::MULTIPLICATION)
                break;

            ++m_it;

            Pointer &pointer = declarator.pointers.emplace_back();
            while (m_it != std::cend(m_tokens)) {
                auto qualifier = consumeTypeQualifier(m_it);
                if (!qualifier.first)
                    break;
                pointer.typeQualifierList.push_back(qualifier.second);
            }
        }

        if (m_it->type() == lexer::TokenType::IDENTIFIER) {
            declarator.identifier = std::get<std::string>(*m_it);
            ++m_it;
        }

        return declarator;
    }

    std::optional<ParameterDeclaration>
    Parser::consumeParameterDeclaration() noexcept {
        auto declarationSpecifiers = consumeDeclarationSpecifiers();
        if (std::empty(declarationSpecifiers.specifiers))
            return std::nullopt;

        auto declarator = consumeDeclarator();

        return ParameterDeclaration{std::forward<DeclarationSpecifiers>(declarationSpecifiers), std::move(declarator)};
    }

} // namespace parser
