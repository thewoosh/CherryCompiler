/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <string>

#include "Source/Parser/Expressions/Expression.hpp"
#include "Source/Parser/Node.hpp"
#include "Source/Parser/Type.hpp"

namespace parser {

    struct VariableDeclarationNode
            : public Node {

        [[nodiscard]] inline
        VariableDeclarationNode(Type &&type, std::string &&identifier, std::unique_ptr<Expression> &&expression) noexcept
                : Node(NodeType::VARIABLE_DECLARATION)
                , m_type(std::move(type))
                , m_identifier(std::move(identifier))
                , m_expression(std::move(expression)) {
        }

        [[nodiscard]] inline
        VariableDeclarationNode(Type &&type, const std::string &identifier, std::unique_ptr<Expression> &&expression) noexcept
                : Node(NodeType::VARIABLE_DECLARATION)
                , m_type(std::move(type))
                , m_identifier(identifier)
                , m_expression(std::move(expression)) {
        }

        [[nodiscard]] inline constexpr const Expression *
        expression() const noexcept {
            return m_expression.get();
        }

        [[nodiscard]] inline constexpr const std::string &
        identifier() const noexcept {
            return m_identifier;
        }

        [[nodiscard]] inline constexpr const Type &
        returnType() const noexcept {
            return m_type;
        }

    private:
        Type m_type;
        std::string m_identifier;
        std::unique_ptr<Expression> m_expression;
    };

} // namespace parser
