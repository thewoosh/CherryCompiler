/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <vector>

#include "Source/Base/CompilerFeatures.hpp"
#include "Source/Parser/NodeType.hpp"

namespace parser {

    struct Node {
        [[nodiscard]] inline constexpr explicit
        Node(NodeType type) noexcept
                : m_type(type) {
        }

        [[nodiscard]] inline constexpr NodeType
        type() const noexcept {
            return m_type;
        }

    private:
        NodeType m_type;
    };

    struct ParentNode
            : public Node {

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR explicit
        ParentNode(NodeType type, std::vector<std::unique_ptr<Node>> &&children={}) noexcept
                : Node(type)
                , m_children(std::move(children)) {
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR std::vector<std::unique_ptr<Node>> &
        children() noexcept {
            return m_children;
        }

        [[nodiscard]] inline const CHERRY_CONSTEXPR_VECTOR std::vector<std::unique_ptr<Node>> &
        children() const noexcept {
            return m_children;
        }
    private:
        std::vector<std::unique_ptr<Node>> m_children;
    };

    struct RootNode
            : public ParentNode {

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR
        RootNode()
                : ParentNode(NodeType::ROOT) {
        }
    };

} // namespace parser
