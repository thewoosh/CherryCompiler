/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace parser {

    enum class NodeType {
        COMPOUND_STATEMENT,
        EXPRESSION_STATEMENT,
        FUNCTION_DECLARATION,
        FUNCTION_DEFINITION,
        RETURN_STATEMENT,
        ROOT,
        VARIABLE_DECLARATION,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(NodeType nodeType) noexcept {
        switch (nodeType) {
            case NodeType::COMPOUND_STATEMENT:      return "compound-statement";
            case NodeType::EXPRESSION_STATEMENT:    return "expression-statement";
            case NodeType::FUNCTION_DECLARATION:    return "function-declaration";
            case NodeType::FUNCTION_DEFINITION:     return "function-definition";
            case NodeType::RETURN_STATEMENT:        return "return-statement";
            case NodeType::ROOT:                    return "root";
            case NodeType::VARIABLE_DECLARATION:    return "variable-declaration";
            default:                                return "(invalid)";
        }
    }

} // namespace parser
