/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace parser {

    struct Declaration {

        std::string name;
    };

    struct DeclarationList {
        std::vector<DeclarationSpecifier> specifiers{};
        std::vector<Declaration> declarations{};
    };

} // namespace parser
