/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Parser/Types/Pointer.hpp"

namespace parser {

    struct Declarator {
        std::vector<Pointer> pointers;
        std::string identifier;

        // TODO incomplete, this should also include arrays and functions
    };

} // namespace parser
