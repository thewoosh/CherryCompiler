/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace parser {

    enum class TypeQualifier {
        CONST,
        RESTRICT,
        VOLATILE,
        US_ATOMIC,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(TypeQualifier typeQualifier) noexcept {
        switch (typeQualifier) {
            case TypeQualifier::CONST: return "const";
            case TypeQualifier::RESTRICT: return "restrict";
            case TypeQualifier::VOLATILE: return "volatile";
            case TypeQualifier::US_ATOMIC: return "_Atomic";
            default: return "(invalid)";
        }
    }

} // namespace parser
