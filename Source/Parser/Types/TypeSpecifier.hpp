/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace parser {

    struct TypeSpecifier {
        enum class Type {
            VOID,
            CHAR,
            SHORT,
            INT,
            LONG,
            FLOAT,
            DOUBLE,
            SIGNED,
            UNSIGNED,
            US_BOOL,
            US_COMPLEX,

            ATOMIC_TYPE,
            STRUCT_OR_UNION,
            ENUM,
            TYPEDEF_NAME

        };

        Type type;
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(TypeSpecifier::Type type) noexcept {
        switch (type) {
            case TypeSpecifier::Type::VOID: return "void";
            case TypeSpecifier::Type::CHAR: return "char";
            case TypeSpecifier::Type::SHORT: return "short";
            case TypeSpecifier::Type::INT: return "int";
            case TypeSpecifier::Type::LONG: return "long";
            case TypeSpecifier::Type::FLOAT: return "float";
            case TypeSpecifier::Type::DOUBLE: return "double";
            case TypeSpecifier::Type::SIGNED: return "signed";
            case TypeSpecifier::Type::UNSIGNED: return "unsigned";
            case TypeSpecifier::Type::US_BOOL: return "_Bool";
            case TypeSpecifier::Type::US_COMPLEX: return "_Complex";

            case TypeSpecifier::Type::ATOMIC_TYPE: return "atomic-type-specifier";
            case TypeSpecifier::Type::STRUCT_OR_UNION: return "struct-or-union-specifier";
            case TypeSpecifier::Type::ENUM: return "enum-specifier";
            case TypeSpecifier::Type::TYPEDEF_NAME: return "typedef-name";

            default: return "(invalid)";
        }
    }

} // namespace parser
