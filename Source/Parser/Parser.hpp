/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>
#include <vector>

#include <cstddef>

#include "Source/Lexer/Token.hpp"
#include "Source/Parser/Forward.hpp"
#include "Source/Parser/Node.hpp"
#include "Source/Parser/Type.hpp"
#include "Source/Parser/Types/ParameterDeclaration.hpp"

namespace parser {

    class Parser {
        RootNode m_rootNode{};

        ParentNode *m_currentBlockLevel{nullptr};

        const std::vector<lexer::Token> &m_tokens;
        std::vector<lexer::Token>::const_iterator m_it;

        [[nodiscard]] std::unique_ptr<ParentNode>
        consumeCompoundStatement() noexcept;

        [[nodiscard]] bool
        consumeDeclaration() noexcept;

        [[nodiscard]] DeclarationSpecifiers
        consumeDeclarationSpecifiers() noexcept;

        [[nodiscard]] Declarator
        consumeDeclarator() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeExpression() noexcept;

        [[nodiscard]] bool
        consumeFunctionDeclaration(Type returnType, const std::string &identifier) noexcept;

        [[nodiscard]] bool
        consumeKeyword(lexer::KeywordType keyword) noexcept;

        [[nodiscard]] std::optional<ParameterDeclaration>
        consumeParameterDeclaration() noexcept;

        [[nodiscard]] bool
        consumePunctuator(lexer::PunctuatorType punctuatorType) noexcept;

        [[nodiscard]] bool
        consumeReturnStatement() noexcept;

        [[nodiscard]] bool
        peekIdentifier() const noexcept;

        [[nodiscard]] bool
        peekKeyword(lexer::KeywordType keyword) const noexcept;

        [[nodiscard]] bool
        peekPunctuator(lexer::PunctuatorType punctuatorType) const noexcept;

        [[nodiscard]] bool
        processToken() noexcept;

    public:
        [[nodiscard]] inline explicit
        Parser(const std::vector<lexer::Token> &tokens) noexcept
                : m_tokens(tokens) {
        }

        static void
        printAST(const Node *node, std::size_t depth=0) noexcept;

        inline void
        printAST() const noexcept {
            printAST(&m_rootNode);
        }

        static void
        printExpression(const Expression *expression, std::size_t depth=0) noexcept;

        [[nodiscard]] inline constexpr RootNode &
        rootNode() noexcept {
            return m_rootNode;
        }

        [[nodiscard]] inline constexpr const RootNode &
        rootNode() const noexcept {
            return m_rootNode;
        }

        [[nodiscard]] bool
        run() noexcept;
    };

} // namespace parser
