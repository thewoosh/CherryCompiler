/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include "Source/Parser/Node.hpp"
#include "Source/Parser/Type.hpp"
#include "Source/Parser/Types/ParameterDeclaration.hpp"

namespace parser {

    struct FunctionDefinitionNode
            : public ParentNode {

        [[nodiscard]] inline
        FunctionDefinitionNode(Type &&returnType, std::string &&identifier,
                               std::vector<ParameterDeclaration> &&parameterDeclarations,
                               std::vector<std::unique_ptr<Node>> &&children) noexcept
                : ParentNode(NodeType::FUNCTION_DEFINITION, std::move(children))
                , m_returnType(std::move(returnType))
                , m_identifier(std::move(identifier))
                , m_parameterDeclarations(std::move(parameterDeclarations)) {
        }

        [[nodiscard]] inline constexpr const std::string &
        identifier() const noexcept {
            return m_identifier;
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR const std::vector<ParameterDeclaration> &
        parameterDeclarations() const noexcept {
            return m_parameterDeclarations;
        }

        [[nodiscard]] inline constexpr const Type &
        returnType() const noexcept {
            return m_returnType;
        }

    private:
        Type m_returnType;
        std::string m_identifier;
        std::vector<ParameterDeclaration> m_parameterDeclarations;
    };

} // namespace parser
