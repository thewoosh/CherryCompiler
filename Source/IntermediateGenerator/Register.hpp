/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <compare>

#include <cstddef> // for std::size_t

namespace intermediate {

    /**
     * Note:
     *   Index 0 is the invalid state
     */
    struct Register {
        std::size_t index;

        [[nodiscard]] inline constexpr
        Register(std::size_t index) noexcept
                : index(index) {
        }

        [[nodiscard]] inline constexpr auto
        operator<=>(const Register &) const noexcept = default;
    };

} // namespace intermediate

