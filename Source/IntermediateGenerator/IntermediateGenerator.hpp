/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>
#include <vector>

#include "Source/Base/CompilerFeatures.hpp"
#include "Source/IntermediateGenerator/FunctionBlock.hpp"
#include "Source/Parser/Forward.hpp"

namespace intermediate {

    class IntermediateGenerator {
        const parser::RootNode *m_rootNode{};
        std::vector<FunctionBlock> m_blocks{};

        [[nodiscard]] bool
        processChildNode(FunctionBlock &, const parser::Node *) noexcept;

        [[nodiscard]] bool
        processFunctionDefinition(const parser::FunctionDefinitionNode *) noexcept;

    public:
        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR explicit
        IntermediateGenerator(const parser::RootNode *rootNode) noexcept
                : m_rootNode(rootNode) {
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR std::vector<FunctionBlock> &
        functionBlocks() noexcept {
            return m_blocks;
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR const std::vector<FunctionBlock> &
        functionBlocks() const noexcept {
            return m_blocks;
        }

        [[nodiscard]] bool
        run() noexcept;
    };

} // namespace intermediate
