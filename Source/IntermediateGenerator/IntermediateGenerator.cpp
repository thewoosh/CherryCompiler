/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IntermediateGenerator/IntermediateGenerator.hpp"

#ifdef CHERRY_USE_PARALLEL
#   include <algorithm>
#   include <execution>
#endif

#include "Source/IntermediateGenerator/Expressions/Expression.cpp"
#include "Source/Parser/Expressions/PrimaryExpression.hpp"
#include "Source/Parser/FunctionDefinitionNode.hpp"
#include "Source/Parser/Node.hpp"
#include "Source/Parser/Nodes/VariableDeclarationNode.hpp"
#include "Source/Parser/ReturnStatementNode.hpp"

namespace intermediate {

    [[nodiscard]] bool
    processReturnStatement(FunctionBlock &block, const parser::ReturnStatementNode *node) noexcept {
        if (node->expression() != nullptr) {
            auto ret = processExpression(&block, node->expression());
            if (!ret.first) {
                logger::trace("IG: ReturnStmt: failed to execute expression");
                return false;
            }

            if (ret.second.has_value()) {
                block.instructions.emplace_back(InstructionType::RETURN, ret.second.value());
                return true;
            }
        }

        block.instructions.emplace_back(InstructionType::RETURN);
        return true;
    }

    [[nodiscard]] bool
    processVariableDeclaration(FunctionBlock &block, const parser::VariableDeclarationNode *node) noexcept {
        if (block.variables.find(node->identifier()) != std::end(block.variables)) {
            logger::error("IG: Redeclaration of variables isn't allowed! Identifier: {}", node->identifier());
            return false;
        }

        if (node->expression()->type() == parser::ExpressionType::PRIMARY_UNSIGNED) {
            const auto value = static_cast<const parser::PrimaryExpression<std::uint64_t> *>(node->expression())->data();
            block.variables.emplace(node->identifier(), VariableState{value});
            return true;
        }

        if (node->expression() == nullptr) {
            const Register reg = block.registerCounter++;
            block.variables.emplace(node->identifier(), VariableState{reg});
            return true;
        }

        const auto result = processExpression(&block, node->expression());
        if (!result.first || !result.second.has_value()) {
            logger::trace("IG: VariableDecl: failed to execute expression");
            return false;
        }

        block.variables.emplace(node->identifier(), VariableState{result.second.value()});
        return true;
    }

    bool
    IntermediateGenerator::processFunctionDefinition(const parser::FunctionDefinitionNode *node) noexcept {
        auto &block = m_blocks.emplace_back();
        block.name = node->identifier();

        for (const auto &parameterDeclaration : node->parameterDeclarations()) {
            if (!std::empty(parameterDeclaration.declarator.identifier)) {
                const Register reg{block.registerCounter++};
                block.variables.emplace(parameterDeclaration.declarator.identifier,
                                        VariableState{reg});
                block.parameters.emplace_back(reg, &parameterDeclaration);
            }
        }

        for (const auto &child : node->children()) {
            if (!processChildNode(block, child.get())) {
                logger::trace("IG: failed to process child node of function \"{}\", node type: {}", block.name,
                              parser::toString(child->type()));
                return false;
            }
        }

        logger::info("IG: function \"{}\" yields {} instruction(s).", block.name,
                     std::size(block.instructions));
        for (const auto &instruction : block.instructions) {
            if (instruction.lhs.index() != 0) {
                if (instruction.rhs.index() != 0) {
                    logger::info("    {} {}, {}", toString(instruction.type), instruction.lhs.toString(),
                                 instruction.rhs.toString());
                } else {
                    logger::info("    {} {}", toString(instruction.type), instruction.lhs.toString());
                }
            } else {
                logger::info("    {}", toString(instruction.type));
            }
        }

        return true;
    }

    bool
    IntermediateGenerator::processChildNode(FunctionBlock &block, const parser::Node *node) noexcept {
        switch (node->type()) {
            case parser::NodeType::VARIABLE_DECLARATION:
                return processVariableDeclaration(block, static_cast<const parser::VariableDeclarationNode *>(node));
            case parser::NodeType::RETURN_STATEMENT:
                return processReturnStatement(block, static_cast<const parser::ReturnStatementNode *>(node));
            default:
                logger::warning("IG: ProcessChildNode Ignoring node of type {}", parser::toString(node->type()));
                return true;
        }
    }

    bool
    IntermediateGenerator::run() noexcept {
        bool success{true};

#ifdef CHERRY_USE_PARALLEL
        std::for_each(std::execution::par,
                      std::cbegin(m_rootNode->children()),
                      std::cend(m_rootNode->children()),
                      [&](const auto &child) {
#else
        for (const auto &child : m_rootNode->children()) {
#endif
            if (child->type() == parser::NodeType::FUNCTION_DEFINITION) {
                if (!processFunctionDefinition(static_cast<const parser::FunctionDefinitionNode *>(child.get()))) {
#ifdef CHERRY_USE_PARALLEL
                    success = false;
#else
                    return false;
#endif
                }
            } else {
                logger::warning("IG: Run: Ignoring node of type {}", parser::toString(child->type()));
            }
#ifdef CHERRY_USE_PARALLEL
        });
#else
        }
#endif

        return success;
    }

} // namespace intermediate
