/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <utility> // for std::move

#include "Source/IntermediateGenerator/InstructionType.hpp"
#include "Source/IntermediateGenerator/Operand.hpp"

namespace intermediate {

    struct Instruction {
        InstructionType type;
        Operand lhs{};
        Operand rhs{};

        [[nodiscard]] inline explicit
        Instruction(InstructionType type) noexcept
                : type(type) {
        }

        [[nodiscard]] inline
        Instruction(InstructionType type, Operand &&lhs) noexcept
                : type(type)
                , lhs(std::move(lhs)) {
        }

        [[nodiscard]] inline
        Instruction(InstructionType type, Operand &&lhs, Operand &&rhs) noexcept
                : type(type)
                , lhs(std::move(lhs))
                , rhs(std::move(rhs)) {
        }
    };

} // namespace intermediate
