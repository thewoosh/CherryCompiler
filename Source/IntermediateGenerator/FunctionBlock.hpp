/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <map>
#include <string_view>
#include <vector>

#include <cstddef> // for std::size_t

#include "Source/IntermediateGenerator/Instruction.hpp"
#include "Source/IntermediateGenerator/VariableState.hpp"
#include "Source/Parser/Types/ParameterDeclaration.hpp"

namespace intermediate {

    struct FunctionBlock {
        std::string name{};

        std::vector<Instruction> instructions{};
        std::size_t registerCounter{1};

        std::map<std::string_view, VariableState> variables{};
        std::vector<std::pair<intermediate::Register, const parser::ParameterDeclaration *>> parameters{};
    };

} // namespace intermediate
