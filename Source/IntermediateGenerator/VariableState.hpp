/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/IntermediateGenerator/Register.hpp"

namespace intermediate {

    struct VariableState
            : public std::variant<Register, std::uint64_t> {
        [[nodiscard]] inline constexpr bool
        isRegister() const noexcept {
            return index() == 0;
        }

        [[nodiscard]] inline constexpr bool
        isImmediateUnsigned() const noexcept {
            return index() == 1;
        }
    };

} // namespace intermediate
