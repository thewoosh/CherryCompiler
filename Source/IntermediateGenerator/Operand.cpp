/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IntermediateGenerator/Operand.hpp"

#include <fmt/format.h>

#include "Source/Base/Assert.hpp"

namespace intermediate {

    std::string
    Operand::toString() const noexcept {
        switch (index()) {
            case 0: return "null";
            case 1: return fmt::format("r{}", std::get<Register>(*this).index);
            case 2: return fmt::format_int(std::get<std::uint64_t>(*this)).str();
            default:
                CHERRY_ASSERT(false);
        }
    }

} // namespace intermediate
