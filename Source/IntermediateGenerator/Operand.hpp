/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <utility> // for std::forward
#include <variant>

#include <cstddef>
#include <cstdint>

#include "Source/IntermediateGenerator/IntermediateValue.hpp"
#include "Source/IntermediateGenerator/Register.hpp"

namespace intermediate {

    struct Operand
            : public std::variant<std::nullptr_t, Register, std::uint64_t> {

        [[nodiscard]] inline constexpr
        Operand() noexcept
                 : variant(nullptr) {
        }

        [[nodiscard]] inline constexpr
        Operand(IntermediateValue value) noexcept {
            if (value.isRegister())
                operator=(std::get<Register>(value));
            else if (value.isImmediateUnsigned())
                operator=(std::get<std::uint64_t>(value));
        }

        [[nodiscard]] inline constexpr
        Operand(Register reg) noexcept
                : variant(reg) {
        }

        [[nodiscard]] inline constexpr
        Operand(std::uint64_t value) noexcept
                : variant(value) {
        }

        [[nodiscard]] inline constexpr bool
        isImmediateUnsigned() const noexcept {
            return index() == 2;
        }

        [[nodiscard]] inline constexpr bool
        isNull() const noexcept {
            return index() == 0;
        }

        [[nodiscard]] inline constexpr bool
        isRegister() const noexcept {
            return index() == 1;
        }

        [[nodiscard]] std::string
        toString() const noexcept;
    };

} // namespace intermediate
