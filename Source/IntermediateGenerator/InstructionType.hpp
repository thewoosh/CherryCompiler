/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace intermediate {

    enum class InstructionType {
        ADD,
        DIVIDE,
        MOVE,
        MULTIPLY,
        RETURN,
        SUBTRACT,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(InstructionType instructionType) noexcept {
        switch (instructionType) {
            case InstructionType::ADD: return "add";
            case InstructionType::DIVIDE: return "divide";
            case InstructionType::MOVE: return "move";
            case InstructionType::MULTIPLY: return "multiply";
            case InstructionType::RETURN: return "return";
            case InstructionType::SUBTRACT: return "subtract";
            default: return "(invalid)";
        }
    }

} // namespace intermediate
