/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IntermediateGenerator/Expressions/BiExpression.hpp"

#include "Source/Base/Logger.hpp"
#include "Source/IntermediateGenerator/Expressions/Expression.hpp"
#include "Source/IntermediateGenerator/FunctionBlock.hpp"
#include "Source/Parser/Expressions/BiExpression.hpp"
#include "Source/Parser/Expressions/PrimaryExpression.hpp"

namespace intermediate {

#define CHERRY_RESOLVE_BI_EXPRESSION(functionName, instructionType) \
    std::pair<bool, std::optional<IntermediateValue>> \
    functionName(const parser::BiExpression *expression, FunctionBlock *block) noexcept { \
        auto lhsResult = processExpression(block, expression->lhs()); \
        if (!lhsResult.first) { \
            logger::trace("IG: failed to resolve lhs of {}", parser::toString(expression->type())); \
            return {false, std::nullopt}; \
        } \
        auto rhsResult = processExpression(block, expression->rhs()); \
        if (!rhsResult.first) { \
            logger::trace("IG: failed to resolve rhs of {}", parser::toString(expression->type())); \
            return {false, std::nullopt}; \
        }                                                           \
        block->instructions.emplace_back(InstructionType::instructionType, lhsResult.second.value(), rhsResult.second.value()); \
        return {true, IntermediateValue{lhsResult.second.value()}}; \
    }

    CHERRY_RESOLVE_BI_EXPRESSION(processMultiplicativeMulExpression, MULTIPLY)
    CHERRY_RESOLVE_BI_EXPRESSION(processMultiplicativeDivExpression, DIVIDE)

    CHERRY_RESOLVE_BI_EXPRESSION(processAdditiveAddExpression, ADD)
    CHERRY_RESOLVE_BI_EXPRESSION(processAdditiveSubExpression, SUBTRACT)

} // namespace intermediate
