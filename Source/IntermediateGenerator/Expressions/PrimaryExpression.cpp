/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IntermediateGenerator/Expressions/PrimaryExpression.hpp"

#include "Source/Base/Logger.hpp"
#include "Source/IntermediateGenerator/FunctionBlock.hpp"
#include "Source/Parser/Expressions/PrimaryExpression.hpp"

namespace intermediate {

    std::pair<bool, std::optional<IntermediateValue>>
    processPrimaryIdentifierExpression(const parser::PrimaryExpression<std::string> *expression,
                                       FunctionBlock *block) noexcept {
        auto it = block->variables.find(expression->data());
        if (it == std::end(block->variables)) {
            return {false, std::nullopt};
        }

        return {true, IntermediateValue{it->second}};
    }

    std::pair<bool, std::optional<IntermediateValue>>
    processPrimaryUnsignedExpression(const parser::PrimaryExpression<std::uint64_t> *expression,
                                     FunctionBlock *) noexcept {
        return {true, IntermediateValue{expression->data()}};
    }

} // namespace intermediate
