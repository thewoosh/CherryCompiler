/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>
#include <string>

#include "Source/IntermediateGenerator/Forward.hpp"
#include "Source/IntermediateGenerator/IntermediateValue.hpp"
#include "Source/Parser/Forward.hpp"

namespace intermediate {

    [[nodiscard]] std::pair<bool, std::optional<IntermediateValue>>
    processPrimaryIdentifierExpression(const parser::PrimaryExpression<std::string> *expression,
                                       FunctionBlock *block) noexcept;

    [[nodiscard]] std::pair<bool, std::optional<IntermediateValue>>
    processPrimaryUnsignedExpression(const parser::PrimaryExpression<std::uint64_t> *expression,
                                     FunctionBlock *block) noexcept;

} // namespace intermediate
