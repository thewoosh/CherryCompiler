/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IntermediateGenerator/Expressions/Expression.hpp"

#include <optional>

#include "Source/IntermediateGenerator/Expressions/BiExpression.hpp"
#include "Source/IntermediateGenerator/Expressions/PrimaryExpression.hpp"
#include "Source/Parser/Expressions/BiExpression.hpp"
#include "Source/Parser/Expressions/PrimaryExpression.hpp"

namespace intermediate {

    std::pair<bool, std::optional<IntermediateValue>>
    processExpression(FunctionBlock *block,
                      const parser::Expression *expression) noexcept {
        switch (expression->type()) {
            case parser::ExpressionType::ADDITIVE_ADD:
                return processAdditiveAddExpression(static_cast<const parser::BiExpression *>(expression), block);
            case parser::ExpressionType::ADDITIVE_SUBTRACT:
                return processAdditiveSubExpression(static_cast<const parser::BiExpression *>(expression), block);
            case parser::ExpressionType::MULTIPLICATIVE_MULTIPLY:
                return processMultiplicativeMulExpression(static_cast<const parser::BiExpression *>(expression), block);
            case parser::ExpressionType::MULTIPLICATIVE_DIVIDE:
                return processMultiplicativeDivExpression(static_cast<const parser::BiExpression *>(expression), block);
            case parser::ExpressionType::PRIMARY_IDENTIFIER:
                return processPrimaryIdentifierExpression(static_cast<const parser::PrimaryExpression<std::string> *>(expression),
                                                          block);
            case parser::ExpressionType::PRIMARY_UNSIGNED:
                return processPrimaryUnsignedExpression(static_cast<const parser::PrimaryExpression<std::uint64_t> *>(expression),
                                                        block);
            default:
                logger::error("IG: unknown expression type: {}", parser::toString(expression->type()));
                return {false, std::nullopt};
        }
    }

} // namespace intermediate
