/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>

#include "Source/IntermediateGenerator/FunctionBlock.hpp"
#include "Source/IntermediateGenerator/IntermediateValue.hpp"
#include "Source/Parser/Expressions/Expression.hpp"

namespace intermediate {

    [[nodiscard]] std::pair<bool, std::optional<IntermediateValue>>
    processExpression(FunctionBlock *block, const parser::Expression *expression) noexcept;

} // namespace intermediate
