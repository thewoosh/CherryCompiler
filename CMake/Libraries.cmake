# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

option(USE_PARALLELIZATION "Parallelize Cherry's generators and optimizers." ON)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR})

find_package(fmt QUIET)
if (NOT fmt_FOUND)
    add_subdirectory(${CMAKE_SOURCE_DIR}/ThirdParty/fmt)
endif()

if (${USE_PARALLELIZATION})
    find_package(TBB REQUIRED)
    target_link_libraries(project_libraries INTERFACE tbb)
    add_compile_definitions(USE_PARALLELIZATION)
endif()
